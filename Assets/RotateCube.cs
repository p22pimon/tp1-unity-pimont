using UnityEngine;
using UnityEngine.InputSystem;
public class RotateCube : MonoBehaviour
{
    private const string Binding = "<Keyboard>/R";

    // Start is called before the first frame update

    public float speed = 3.0f;
    public InputAction activateRotation;
    private bool rot = false;

    void Start(){
        activateRotation = new InputAction(binding: Binding);
        activateRotation.Enable();
        activateRotation.started += ctx => rot = !rot;
        
    }


    // Update is called once per frame
    void Update(){
        if (Input.GetKeyDown(KeyCode.R)){
            Debug.Log("Touche R utilisée");
        }
        if (rot){
            transform.Rotate(0, speed, 0);
        }
        
    }
}


