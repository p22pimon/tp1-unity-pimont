using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{

    private Rigidbody gravity;
    private Renderer rend;
    private Color initialColor;

    //Detect if a click occurs

    public void OnPointerClick(PointerEventData eventData)
    {
        float force = 20000f;
        Vector3 direction = Camera.main.transform.forward;
        gravity.AddForce(direction*force);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Color r = Color.red;
        rend.material.color = r;
    }

    //Detect when Cursor leaves the GameObject
    public void OnPointerExit(PointerEventData eventData)
    {
        rend.material.color = initialColor;
    }

    // Start is called before the first frame update
    void Start()
    {
       gravity = transform.GetComponent<Rigidbody>();
       rend = transform.GetComponent<Renderer>();
       initialColor = rend.material.color; 
    }

    // Update is called once per frame
    void Update()
    {

        
    }
}
